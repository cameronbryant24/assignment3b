package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean
@ViewScoped
public class Orders {
List<Order> orders = new ArrayList<Order>();

//sets and populates what the orders are
	public Orders() {
		orders.add(new Order("00000", "This is Product 1", 1.00f, 1));
		orders.add(new Order("00001", "This is Product 2", (float)2.00, 2));
		orders.add(new Order("00002", "This is Product 3", (float)3.00, 3));
		orders.add(new Order("00003", "This is Product 4", (float)4.00, 4));
		orders.add(new Order("00004", "This is Product 5", (float)5.00, 5));
		orders.add(new Order("00005", "This is Product 6", (float)6.00, 6));
		orders.add(new Order("00006", "This is Product 7", (float)7.00, 7));
		orders.add(new Order("00007", "This is Product 8", (float)8.00, 8));
		orders.add(new Order("00008", "This is Product 9", (float)9.00, 9));
		orders.add(new Order("00009", "This is Product 10", (float)10.00, 10));
	}
	
	//gives out the order list
	public List<Order> getOrders() {
		return orders;
	}
	
	//sets what the order is and keeps what is populated
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
}
