package beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class Order {

	//setting the parameters for the order
	String orderNo = "";
	String productName = "";
	float price = 0;
	int quantity = 0;
	
	public Order(String orderNo, String productName, float price, int quantity) {
		this.orderNo = orderNo;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
	}
	
	//gets the number of the order
	public String getOrderNo() {
		return orderNo;
	}
	
	//sets what the order number is going to be
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	//gets the name of the product
	public String getProductName() {
		return productName;
	}

	//sets the name of the product
	public void setProductName(String productName) {
		this.productName = productName;
	}

	//gets the price of the product
	public float getPrice() {
		return price;
	}

	//sets the price of the product
	public void setPrice(float price) {
		this.price = price;
	}

	//gets the quantity for the order
	public int getQuantity() {
		return quantity;
	}

	//sets the quantity of the order
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
