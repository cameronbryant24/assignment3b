package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ManagedBean
@ViewScoped
public class User {
	//if a managed bean is not null then it requires the user to enter a first name that fits the requirements of the @size
	@NotNull(message = "Please enter a First Name. This is a required field.")
	@Size(min=4, max=15)
	private String firstName = "";
	
	//if a managed bean is not null then it requires the user to enter a last name that fits the requirements of the @size
	@NotNull(message = "Please enter a Last Name. This is a required field.")
	@Size(min=4, max=15)
	private String lastName = "";

	//sets what each of these aspects are in the beginning without needing the immediate user input
	public User() {
		firstName="Cameron";
		lastName="Bryant";
	}

	//gets the first name
	public String getFirstName() {
		return firstName;
	}

	//sets the first name
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	//gets the last name
	public String getLastName() {
		return lastName;
	}

	//sets the last name
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
